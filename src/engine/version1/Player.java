package engine.version1;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Player {
			int x;
			int y;
			
			int vx,vy,initialy=0;
			double dt;
			double dx,dy=467,dvx,dvy;
			boolean jumping,standing=true,walking,facingRight=true,facingLeft=false,justStartedJump=true;
			int sizex,sizey;
			JLabel stand[],move[],jump[];
			JLabel currentStand;
			double standTimer;
			int standIndex,walkIndex,standCounter;
			
			Player(){
				stand= new JLabel[8];
				jump= new JLabel[2];
				move= new JLabel[16];
				assignImageToPlayer();
				currentStand=stand[0];
				 x=0;
				 y=467;
				 vy=0;
				 vx=0;
				 sizex=0;
				 sizey=0;

			}
			
			/*public JLabel getPlayerIcon(){
				if(standing)return stand;
				if(walking) return move;
				else return jump;
			}*/
			private void assignImageToPlayer(){
				stand[0]=importImage("obj_Idle000.png");
				stand[1]=importImage("obj_Idle001.png");
				stand[2]=importImage("obj_Idle002.png");
				stand[3]=importImage("obj_Idle003.png");
				stand[4]=importImage("obj_Idle100.png");
				stand[5]=importImage("obj_Idle101.png");
				stand[6]=importImage("obj_Idle102.png");
				stand[7]=importImage("obj_Idle103.png");
				jump[0]=importImage("obj_JumpHigh000.png");
				jump[1]=importImage("obj_JumpHigh100.png");
				move[0]=importImage("obj_Walk000.png");
				move[1]=importImage("obj_Walk001.png");
				move[2]=importImage("obj_Walk002.png");
				move[3]=importImage("obj_Walk003.png");
				move[4]=importImage("obj_Walk004.png");
				move[5]=importImage("obj_Walk005.png");
				move[6]=importImage("obj_Walk006.png");
				move[7]=importImage("obj_Walk007.png");
				move[8]=importImage("obj_Walk100.png");
				move[9]=importImage("obj_Walk101.png");
				move[10]=importImage("obj_Walk102.png");
				move[11]=importImage("obj_Walk103.png");
				move[12]=importImage("obj_Walk104.png");
				move[13]=importImage("obj_Walk105.png");
				move[14]=importImage("obj_Walk106.png");
				move[15]=importImage("obj_Walk107.png");
			//	move =importImage(walking);
				//jump =importImage(jumping);
				
			}
			public void setXVelocity(int v){
				vx=v;
							}
			
			public void resetXVelocity(){
				vx=0;
			}
			
			public void setYVelocity(int v){
				vy=v;
			}
			public void resetYVelocity(int v){
				//vy=0;
			}
			
			public void updateStand(){
				if(standing){
					standCounter++;
					if(standCounter==3){
				if(standing && facingRight){

				standIndex++;
				if(standIndex>3)standIndex=0;
				currentStand=stand[standIndex];
				for(int i=0;i<8;i++)stand[i].setVisible(false);
				for(int i=0;i<2;i++)jump[i].setVisible(false);
				for(int i=0;i<16;i++)move[i].setVisible(false);
				this.currentStand.setLocation(x,y);
				currentStand.setVisible(true);
				}
				if(standing && !facingRight){
					if(standIndex<4)standIndex=4;
					standIndex++;
				if(standIndex>7){standIndex=4;}
				currentStand=stand[standIndex];
				for(int i=0;i<8;i++)stand[i].setVisible(false);
				for(int i=0;i<2;i++)jump[i].setVisible(false);
				for(int i=0;i<16;i++)move[i].setVisible(false);
				this.currentStand.setLocation(x,y);
				currentStand.setVisible(true);
				}
				standCounter=1;
				}
				}
				if(jumping&&facingRight){
					for(int i=0;i<8;i++)stand[i].setVisible(false);
					for(int i=0;i<2;i++)jump[i].setVisible(false);
					for(int i=0;i<16;i++)move[i].setVisible(false);
					currentStand=jump[0];
					this.currentStand.setLocation(x,y);
					currentStand.setVisible(true);
				}
				
				if(jumping&&!facingRight){
					for(int i=0;i<8;i++)stand[i].setVisible(false);
					for(int i=0;i<2;i++)jump[i].setVisible(false);
					for(int i=0;i<16;i++)move[i].setVisible(false);
					currentStand=jump[1];
					this.currentStand.setLocation(x,y);
					currentStand.setVisible(true);
				}
				
				if(walking&&facingRight){
					walkIndex++;
					if(walkIndex>7)walkIndex=0;
					for(int i=0;i<8;i++)stand[i].setVisible(false);
					for(int i=0;i<2;i++)jump[i].setVisible(false);
					for(int i=0;i<16;i++)move[i].setVisible(false);
					currentStand=move[walkIndex];
					this.currentStand.setLocation(x,y);
					currentStand.setVisible(true);
				}
				if(walking&&!facingRight){
					if(walkIndex<8)walkIndex=8;
					walkIndex++;
					if(walkIndex>15)walkIndex=8;
					for(int i=0;i<8;i++)stand[i].setVisible(false);
					for(int i=0;i<2;i++)jump[i].setVisible(false);
					for(int i=0;i<16;i++)move[i].setVisible(false);
					currentStand=move[walkIndex];
					this.currentStand.setLocation(x,y);
					currentStand.setVisible(true);
				}
			}
			
			public void setJumping(){
				standing=false;
				walking=false;
				jumping=true;
			}
			public void setStanding(){
				standing=true;
				walking=false;
				jumping=false;
			}
			
			public void setWalking(){
				standing=false;
				walking=true;
				jumping=false;
			}
			public void update(double delta){
				dt=delta;
				standTimer+=dt;
				if(standTimer>=0.1){
					updateStand();
					standTimer=0;
				}
				if(vx!=0){
					System.out.printf("Move %f, x:%d y:%d\n",vx*dt,x,y);
					if(vx>0){
						if(standing){
							setWalking();
						}
						boolean tmp=facingRight;
						facingRight=true;
						if(!tmp)updateStand();
						}
					if(vx<0){
						if(standing){
							setWalking();
						}
						boolean tmp=facingRight;
						facingRight=false;
						if(tmp)updateStand();
						}
					if(!wall()){
				dx+=vx*dt*160;//se vx = 1 si muove a 160 px al secondo
				x=(int)dx;
				this.currentStand.setLocation(x, (int) this.currentStand.getLocation().getY());}
				if(!ground())setJumping();
				}
				else{
					if(!jumping)setStanding();
				}
				/*if(dy>=467&&vy!=0){
					vy=0;
					dvy=0;
				}*/
				
				if(jumping){
					if(justStartedJump){updateStand();justStartedJump=false;}
					System.out.printf("Jump %f y:%d vy:%d\n",vy*dt,y,vy);
					if(dvy==0)dvy=vy;
					dvy+=9.8*dt*50;
					vy=(int)dvy;
					dy+=dvy*dt;
					y=(int)dy;
					this.currentStand.setLocation(x,y);
				}
				
			if(ground()&&jumping){

				vy=0;
				dvy=0;
				setStanding();
				updateStand();
			}
			}
			
			private boolean ground(){
				if(this.dy>=467&&x<500){dy=467;y=467;return true;}
				if(this.dy>=600&&x>500){dy=600;y=600;return true;}
						return false;
			}
			
			private boolean wall(){
				if(this.x>500&&x<505&&this.y>467&&this.vx<0)return true;
				if(this.x<712&&x>707&&this.vx>0)return true;
				if(this.x>-50&&x<-40&&this.vx<0)return true;
				return false;
			}
			private JLabel importImage(String path){
				JLabel icona;
		        final ImageIcon icon = createImageIcon(String.format("images/%s",path));
		        Image img = icon.getImage();
		        BufferedImage bi = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		        Graphics g = bi.createGraphics();
		        g.drawImage(img, 0, 0, 128, 128, null);
		        ImageIcon newIcon = new ImageIcon(bi);
		        icona=new JLabel(newIcon);
		        icona.setBounds(15, 225,
		                newIcon.getIconWidth(),
		                newIcon.getIconHeight());
		        return icona;
			}
			
		    protected static ImageIcon createImageIcon(String path) {
		        java.net.URL imgURL = Main.class.getResource(path);
		        if (imgURL != null) {
		            return new ImageIcon(imgURL);
		        } else {
		            System.err.println("Couldn't find file: " + path);
		            return null;
		        }
		    }
}
