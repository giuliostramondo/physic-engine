package engine.version1;

public class PhysicsUtilities {
	   private static long lastTime = System.nanoTime();

	    public static double getTimeDelta(){
	        return (System.nanoTime()-lastTime)/1000000000d;
	    }

	    public static void updateDelta(){
	        lastTime = System.nanoTime();
	    }
}
