package engine.version1;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;


public class Main extends JPanel implements ActionListener, KeyListener, MouseListener, MouseMotionListener{

	private static final long serialVersionUID = 1L;
	private JLayeredPane layeredPane;
	private static Player p;
    public JLabel StatusBar;
    public JLabel bgimg;
    public JLabel bgbrick;
    public JLabel bgbrick2;
    public static PhysicEngine engine;
    public static int xv=1,yv=500;
	public Main() {

	    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	    layeredPane = new JLayeredPane();
	    layeredPane.setPreferredSize(new Dimension(800, 800));
	    layeredPane.setBorder(BorderFactory.createTitledBorder("Engine v1"));
	    addMouseListener(this);
	    addMouseMotionListener(this);
	    addKeyListener(this);
	    bgimg=importImage("bg-texture.jpg",800,800);
	    bgbrick=importImage("12-Brick-Texture.jpg",550,297);
	    bgbrick2=importImage("12-Brick-Texture.jpg",550,297);
	    layeredPane.add(bgimg,new Integer(0));
	    layeredPane.add(bgbrick,new Integer(1));
	    layeredPane.add(bgbrick2,new Integer(1));
	    bgimg.setLocation(0, 0);
	    bgbrick.setLocation(0, 589);
	    bgbrick2.setLocation(500, 722);
	    layeredPane.add(p.currentStand, new Integer(2));
	    layeredPane.add(p.stand[1], new Integer(2));
	    layeredPane.add(p.stand[2], new Integer(2));
	    layeredPane.add(p.stand[3], new Integer(2));
	    layeredPane.add(p.stand[4], new Integer(2));
	    layeredPane.add(p.stand[5], new Integer(2));
	    layeredPane.add(p.stand[6], new Integer(2));
	    layeredPane.add(p.stand[7], new Integer(2));
		layeredPane.add(p.jump[0], new Integer(2));
		layeredPane.add(p.jump[1], new Integer(2));
		for(int i=0;i<16;i++)layeredPane.add(p.move[i], new Integer(2));
	    add(Box.createRigidArea(new Dimension(0, 10)));
	    add(createControlPanel());
	    add(Box.createRigidArea(new Dimension(0, 10)));
	    add(layeredPane);

	}
	public static void main(String[] args) {
		p=new Player();
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }

        });
        engine = new PhysicEngine(p);
        new Thread(engine).start();
        
        
		

	}
	

    public void mouseMoved(MouseEvent e) {
      	//StatusBar.setText(String.format("Muose at x = %d, y= %d\n", e.getX(),e.getY()));
    	

      	
    }
    //800x800
	private JLabel importImage(String path,int dimX,int dimY){
		JLabel icona;
        final ImageIcon icon = createImageIcon(String.format("images/%s",path));
        Image img = icon.getImage();
        BufferedImage bi = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics g = bi.createGraphics();
        g.drawImage(img, 0, 0, dimX	, dimY, null);
        ImageIcon newIcon = new ImageIcon(bi);
        icona=new JLabel(newIcon);
        icona.setBounds(15, 225,
                newIcon.getIconWidth(),
                newIcon.getIconHeight());
        return icona;
	}
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = Main.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Physic Engine");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        //Create and set up the content pane.
        JComponent newContentPane = new Main();
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);
 
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    private JPanel createControlPanel() {
    	JLabel statusbar = new JLabel("Qui si scrivono cose");
        JPanel controls = new JPanel();
        controls.add(statusbar);
    	StatusBar=statusbar;
        controls.setBorder(BorderFactory.createTitledBorder(
                                 "Informazioni"));
        return controls;
    }
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
	    this.requestFocusInWindow();
		   if( this.hasFocus())System.out.print("has focus");
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		StatusBar.setText(String.format("Pressed char: %d\n", e.getKeyCode()));
		//this.StatusBar.settext("Pressed: %c",arg0.getKeyChar());
		if(e.getKeyCode()==39){StatusBar.setText(String.format("Pressed right\n"));
								p.setXVelocity(1*xv);}
		if(e.getKeyCode()==37){StatusBar.setText(String.format("Pressed left\n"));
		p.setXVelocity(-1*xv);}
		
		if(e.getKeyCode()==32){StatusBar.setText(String.format("Pressed jump\n"));
		p.setYVelocity(-1*yv);
		p.setJumping();
		p.justStartedJump=true;}
		//if(e.getKeyCode()=='%'){StatusBar.setText(String.format("Pressed left\n"));}
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		StatusBar.setText(String.format("Released char: %c\n", e.getKeyChar()));
		if(e.getKeyCode()==39){
		p.resetXVelocity();
		StatusBar.setText(String.format("Released right vx:%d vy:%d\n",p.vx,p.vy));}
		if(e.getKeyCode()==37){
			p.resetXVelocity();
			StatusBar.setText(String.format("Released left vx:%d vy:%d\n",p.vx,p.vy));}
		System.out.print("key pressed");
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	//	StatusBar.setText(String.format("typed char: %c\n", e.getKeyChar()));
		//System.out.print("key pressed");
	}
	
	
}
