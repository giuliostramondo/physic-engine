package engine.version1;

class PhysicEngine implements Runnable {
    Player p;
    boolean status=true;
    private static long lastTime = System.nanoTime();
    PhysicEngine(Player p1) {
        p=p1;
    }

    public void run() {
    	
    	while(status){
    	//	System.out.println("Ciclo");

    	p.update(getTimeDelta());
    	updateDelta();
    	try {
			Thread.sleep(10);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
    }
    	}


	    public static double getTimeDelta(){
	        return (System.nanoTime()-lastTime)/1000000000d;
	    }

	    public static void updateDelta(){
	        lastTime = System.nanoTime();
	    }
}